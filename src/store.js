import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: "finstattool",
  storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    analysisProjects: [
      { 
        id: 1, 
        title: "Dein erstes Analyse Projekt", 
        dataSources: [
          {
            name: "Erste Datenquelle für 1. Projekt",
            headers: ["Valuta-Datum", "Betreff", "Begünstigter", "Betrag"],
            rows: []
          }
        ] 
      },
      { 
        id: 2, 
        title: "Dein zweites Analyse Projekt", 
        dataSources: [
          {
            name: "Erste Datenquelle für 2. Projekt",
            headers: ["Valuta-Datum", "Betreff", "Begünstigter", "Betrag"],
            rows: []
          }
        ] 
      }
    ],
    currentProject: { 
      id: 0, 
      title: "[empty]", 
      dataSources: [] 
    },
    emptyProject: { 
      id: 0, 
      title: "[empty]", 
      dataSources: [] 
    }
  },
  mutations: {
    setCurrentProject(state, project) {
      state.currentProject = project;
    },
    saveAnalysisProject(state, project) {
      let p = state.analysisProjects.find(o => o.id === project.id);
      p.title = project.title;
      p.dataSources = project.dataSources;
      for (let ds of p.dataSources) {
        if (!ds.id) {
          ds.id = p.dataSources.reduce(function(a, b){
            return Math.max(a, b.id || 0);
          }, 0) + 1;
        }
      }
    }
  },
  actions: {

  },
  plugins: [vuexPersist.plugin]
})
