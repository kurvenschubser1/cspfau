import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import AnalysisProjectView from './views/AnalysisProjectView.vue'
import AnalysisProjectStats from './components/AnalysisProjectStats.vue'
import AnalysisProjectData from './components/AnalysisProjectData.vue'
import AnalysisProjectDataSources from './components/AnalysisProjectDataSources.vue'
import AnalysisProjectDataSource from './components/AnalysisProjectDataSource.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/projects',
      name: 'projects',
      component: Home
    },
    {
      path: '/project/:id',
      name: 'project',
      component: AnalysisProjectView,
      children: [
        {
          path: "stats",
          name: "project-stats",
          component: AnalysisProjectStats
        },
        {
          path: "data",
          name: "project-data",
          component: AnalysisProjectData
        },
        {
          path: "datasources",
          name: "project-datasources",
          component: AnalysisProjectDataSources
        },
        {
          path: "datasource/:dsid",
          name: "project-datasource",
          component: AnalysisProjectDataSource
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
